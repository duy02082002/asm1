﻿using Assignment1;

int soLuong = 0;
do
{
    try{
        Console.WriteLine("Nhap so luong giao vien: ");
        soLuong = Int32.Parse(Console.ReadLine());
    }
    catch (Exception e)
    {
        Console.WriteLine("wrong format");
        soLuong = 0;
    }
 
        if (soLuong == null || soLuong <= 0) Console.WriteLine("vui long nhap so luong lon hon 0");
        else
        {
            List<GiaoVien> dsGiaoVien = new List<GiaoVien>();
            string hoTen = null;
            int namSinh = 0;
            double luongCoBan = 0;
            double heSoLuong = 0;

            for (int i = 0; i < soLuong; i++)
            {
                GiaoVien gv = new GiaoVien();
                Boolean check = true;
                do
                {
                    Console.WriteLine("Nhap ho ten giao vien thu " + (i + 1) + ": ");
                    hoTen = Console.ReadLine();
                    if (hoTen.Length != 0) check = false;
                    else
                    {
                        Console.WriteLine("Vui long nhap day du.");
                        check = true;
                    }
                } while (check);

                do
                {
                try
                {
                    Console.WriteLine("Nhap nam sinh giao vien thu " + (i + 1) + ": ");
                    namSinh = Int32.Parse(Console.ReadLine());
                    check = false;
                }catch (Exception e)
                {
                    Console.WriteLine("Vui long nhap day du.");
                    check = true;
                }
                } while (check);

                do
                {
                try
                {
                    Console.WriteLine("Nhap luong co ban cua giao vien thu " + (i + 1) + ": ");
                    luongCoBan = double.Parse(Console.ReadLine());
                    check = false;
                }catch (Exception e)
                {
                    Console.WriteLine("Vui long nhap day du.");
                    check = true;
                }
                } while (check);

                do
                {
                try
                {
                    Console.WriteLine("Nhap he so luong giao vien thu " + (i + 1) + ": ");
                    heSoLuong = double.Parse(Console.ReadLine());
                    check = false;
                }catch (Exception e)
                {
                    Console.WriteLine("Vui long nhap day du.");
                    check = true;
                }
                } while (check);

                gv.NhapThongTin(hoTen, namSinh, luongCoBan);
                gv.NhapThongTin(heSoLuong);
                dsGiaoVien.Add(gv);
            }
            GiaoVien temp = null;
            foreach (GiaoVien gv in dsGiaoVien)
            {
                if (temp == null) temp = gv;
                if (gv.TinhLuong() < temp.TinhLuong()) temp = gv;
            }
            Console.WriteLine("Giao vien co luong thap nhat: \n" + temp.XuatThongTin());
            Console.Read();
        }
    
}while(soLuong <= 0);
