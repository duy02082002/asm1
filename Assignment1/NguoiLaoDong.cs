﻿namespace Assignment1
{
    internal class NguoiLaoDong
    {
        public string HoTen;
        public int NamSinh;
        public double LuongCoBan;
        public NguoiLaoDong()
        {
        }

        public NguoiLaoDong(string hoTen, int namSinh, double luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
        }
        public void NhapThongTin(string hoTen, int namSinh, double luongCoBan)
        {
                this.HoTen = hoTen;
                this.NamSinh = namSinh;
                this.LuongCoBan = luongCoBan;
        }
        public double TinhLuong()
        {
            return this.LuongCoBan;
        }
        public String XuatThongtin()
        {
            return "Ho ten la: {" + HoTen + "}, " +
                "nam sinh: { " + NamSinh + " }, " +
                "luong co ban: { " + LuongCoBan + " }.";
        }
    }
}