﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    internal class GiaoVien : NguoiLaoDong
    {
        public double HeSoLuong;

        public GiaoVien()
        {
        }

        public GiaoVien(string hoTen, int namSinh, double luongCoBan, double heSoLuong) : base(hoTen, namSinh, luongCoBan)
        {
            HoTen = hoTen;
            NamSinh = namSinh;
            LuongCoBan = luongCoBan;
            HeSoLuong = heSoLuong;
        }
        public void NhapThongTin(double heSoLuong)
        {
            HeSoLuong = heSoLuong;
        }

        public double TinhLuong()
        {
            return LuongCoBan * HeSoLuong * 1.25;
        }
        public string XuatThongTin()
        {
            return "Ho ten la: {" + HoTen + "}, " +
                "nam sinh: { " + NamSinh + " }, " +
                "luong co ban: { " + LuongCoBan + " }, " +
                "luong: { " + TinhLuong() + " }.";
        }
        public void XuLy()
        {
            HeSoLuong = HeSoLuong + 0.6;
        }
    }
}
